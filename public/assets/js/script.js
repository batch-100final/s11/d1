// Sir question pero di related sa javascript

// Pano po yung nag sstay yung tab sa sublime kasi minsan nag sstay sakin minsan hindi need ko pa iclick sa folders tab

// Sir kunwari si form pag mag store kami ng value iistore namin siya kay null?


// alert('hello world');

// syntax contains the rules needed to create a statement in a programming language statements are instructions we tell the application to perform.

// statements end with a semicolon as best practice


 /*

variable ad constants

variables and constants are containers for data that can used to store information and retrieve or manipulate in the future

variables can contain values that can change as the program runs

constants contain values that cannot change as the program runs

To Create a variable, we use the "let" keyword (keywords are special words in a programming language)

To create a constant, we use the "const" keyword

  */


  let productName = "Desktop computer"; //"Desktop computer" is a string, usually have '' or "" to indicate they are strings
  let productPrice = 18999 //18999 is a number, number do not need "" or '' to indicate they are numbers
  const PI = 3.1416 //PI is a const because the value of PI can never change

  /* 
Rules in naming variables and constants:
1. Variable names start with lowercase letters, for multiple wods, we use camelCase
2. Variable names should be descriptive of what values it can contain

  */

  /*

console is a part of a browser wherein outputs can be displayed. It can be accessed via the console tab in any browser

To output a value in the console, we use the console.log functin

  */

  console.log(productName); //console is an object in JS and .log is a function that allows writing of the output
  console.log(productPrice);
  console.log(PI);

  console.log("Hello World");
  console.log(12345);
  console.log("I am selling a " + productName);

  /* 

Data types - are type of data a variable can handle

1. string - a combination of aplhanumric values ( ex. Terence, password1234)
2. number - Positive, negative or decimal values
3. boolean - trutch values ex true or false
4. bigInt  *(not used often)
5. Symbol  *(not used often)
6. Undefined - only appears if a variable was not assigned any value
7. Null - the variable is intentionally left to have a null value
8. Object - combination f data with key-values

  */

  //strong
  let fullName = "Brandon B. Brandon";
  let schoolName = "Zuitt Coding bootcamp";
  let userName = "brandon00";

  //number
  let age = 28;

  //boolean
  let isSingle = true;

  //undefined
  let petName;

  //Null
  let grandChildName=null;

  //Object
  let person = {
 	firstName: "Jobert",
 	lastName: "Boyd",
 	age: 15,
 	petName: "Whitey"
  };




/* 

operators - allow us to perform operations or evaluate results
There are 5 main types of operators:

1. Assignment
2. Arithmetic
3. Comparison
4. Relational
5. Logical

*/

//assignment operator uses the = symbol -> it assigns a value to a variable

let num1 = 28;
let num2 = 75;

//arithmetic operator has 5 operations
let sum = num1 + num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num2 / num1;
console.log(quotient);
let remainder = num2 % num1;
console.log(remainder);

//you can actually combne the assignment and arithmetic operators

let num3 = 17;
// num3 = num3 - 4;
// the line num3 = num3 - 4 can be reduced to this line:
num3 -= 4; //this is the same as num3 = num3 - 4; this is a shorthand version

//Q: what would be the value of num3?
//Q: what is the shorthand version of this statement?
// x = x /y -> x /=y

let num4 = 5;
num4++;
console.log(num4);
// research homework: there is a difference between num++ and ++num. please find out and explain tomorrow.


//comparison and relational
// comparison compares two values if they are equal or not
// ==, ===, != , !==

let numA = 65;
let numB = 65;
console.log(numA == numB);

// let statement1 = "true";
// let statement2 = true;
// let statement3 = statement1 == statement2;
// console.log(statement3);
// let statement4 = statement1 === statement2;

//relational compare two numbers if they are equal or not
// >, <, >=, <=

let numC = 25;
let numD = 45;

console.log(numC > numD);
console.log(numC < numD);
console.log(numC >= numD);
console.log(numC <= numD);


//logical compares two boolean values
//AND &&, or ||, NOT !

let isTall = false;
let isDark = true;
let isHandsome =true;
let didPassStandard = isTall && isDark; //botch conditions sohuld be true
console.log(didPassStandard); // false

let didPassLowerStandard = isTall || isDark; //Needs only at least 1 one condition to be true
console.log(didPassLowerStandard); //true

console.log(!isTall); // true -> ! reverses the value
//Note you can combine comparison, relational and logical operators

let result = isTall || (isDark && isHandsome);
console.log(result);


//

// function createFullName (firstName, middlName, lastName) {
// 	return firtName + '' + middleName + '' + lastName;
// }

// let fullName = createFullName ('Juan', 'Dela', 'Cruz');
// console.log(fullName);

//functions - are groups of statements that performs a single action to prevent code duplication
//A function has two main concepts
//1. Function declaration (definition) - defines what the function does
//2. Function invocation (calling) - using the function

//function declaration
function createFullName(fName, mName, lName){
	return fName + mName + lName;
}

/* 

function is a keyword in JS that says that it is a function
 createFullName is the name of the function, it should descriptive of what id does
 fName, mName, lName are called parameters, these are the inputs that the functon needs in order to work.
 Note: parameters are optional, meaning you can create a function without any parameters

 fName, mName, lName are placeholders for values and is only used inside the function

 return is a keyword in JS that returns the resultive value after the function is completely run
 Note: this is also optional if you don't want any returning values. 

*/

//Function invocation
let fullName1 = createFullName("Brandon", "Ray", "Smith");
console.log (fullName1); //BrandonRaySmith 
let fullName2 = createFullName("John", "Robert", "Smith");
console.log(fullName2); //JohnRobertSmith

let fN = "Jobert";
let mN = "Bob";
let lN = "Garcia";

let fullName3 = createFullName(fN, mN, lN);
console.log(fullName3); //JobertBobGarcia
let fullName4 = createFullName(mN, fN, lN);
console.log(fullName4);

//Mini exercise
//Create a function that gets two numbers and returns twice the sum of the two numbers
//the function name is addTwo.

function addTwo(n1, n2){
	return 2*(n1 + n2) ;
}

console.log(addTwo(54, 106));
console.log(addTwo(0,1));